#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <cstring>
#include <cstdio>
using namespace std;

ofstream file;

void Comp(string);
void Dest(string);
void Jump(string);

	void A(string text){  //function A with string type text parameter
		char Array[16];   //Declaring a character type arrray of size 16
		int b=0, NUM;     //Declaring integer type variables NUM & b and initializing b=0
		int BINARY[16];   //Declaring an integer type array of size 16
        
		if (text[0] == '@'){   } //checking if the instruction is A or not
		else{
	    return;        //if it's not A instruction than return
   }   
			
		int array[16]; //Declaring an integer type array of size 16
		for(int i=0; i<16; i++){  //storing 0 in array
		array[i] = 0;
	}
		for(int i=1; text[i]!=0; i++){  
		if(text[i]== ' '){ // check for ignoring the spaces
		continue;
	}
		Array[b++] = text[i]; //storing all numbers from .asm file to character type Array
	}
		NUM = atoi(Array);  //using function atoi to convert string in to integer 
		if (NUM > 32768){  //if number is greator than 15 bits than return
			return;
		}
		int c; //declaring integer type variable c
		for (c=0; NUM>0 ; c++){  //converting number from decimal to binary
		BINARY[c] = NUM%2;
		NUM = NUM/2;
	}
		int l, m=15; //Declaring variables l & m and initializing m=15
		for(int l=0; l<c; l++){  
			array[m--] = BINARY[l];  //This loop will be saving the binary of the number in the integer array starting from end
	}
		for (int i=0; i<16; i++){
		file << array[i];	//writing binary in file
	}
		file << '\n';   // printing new line in file
		
	}
	
// C-INSTRUCTION FUNCTION

void C(string text){
	if(text[0]=='@')return;         //checking if it is A or not
	for(int i=0;text[i]!=0;i++){     
		if(text[i]=='='){
		string DEST(text.begin(),text.begin()+text.find('=')),COMP(text.begin()+text.find('=')+1,text.end());  //string function starting from beginning to where it finds =
		Comp(COMP);
		Dest(DEST);
		
	}
		if(text[i]==';'){
			string COMP(text.begin(),text.begin()+text.find(';')),JUMP(text.begin()+text.find(';')+1,text.end());
		Comp(COMP);
		Jump(JUMP);
		}
	}
	return;
}
	void Comp(string COMP){
		string c;
		int a;
		if(COMP=="0"){
			a=0;
			  c ="101010";
			
		}
		else if(COMP=="1"){
			a=0;
			  c ="111111";
			
		}
		else if(COMP=="-1"){
			a=0;
			  c ="111010";
			
		}
		else if(COMP=="D"){
			a=0;
			  c ="001100";
			
		}
		else if(COMP=="A"){
			a=0;
			  c ="110000";
			
		}
		else if(COMP=="!D"){
			a=0;
			  c ="001101";
			
		}
		else if(COMP=="!A"){
			a=0;
			  c ="110001";
			
		}
		else if(COMP=="-D"){
			a=0;
			  c ="001111";
			
		}
		else if(COMP=="-A"){
			a=0;
			  c ="110011";
			
		}
		else if(COMP=="D+1"){
			a=0;
			  c ="011111";
			
		}
		else if(COMP=="A+1"){
			a=0;
			  c ="110111";
			
		}
		else if(COMP=="D-1"){
			a=0;
			  c ="001110";
			
		}
		else if(COMP=="A-1"){
			a=0;
			  c ="110010";
			
		}
		else if(COMP=="D+A"){
			a=0;
			  c ="000010";
			
		}
		else if(COMP=="D-A"){
			a=0;
			  c ="010011";
			
		}
		else if(COMP=="A-D"){
			a=0;
			  c ="000111";
			
		}
		else if(COMP=="D&A"){
			a=0;
			  c ="000000";
			
		}
		else if(COMP=="D|A"){
			a=0;
			  c ="010101";
			
		}
		else if(COMP=="M"){
			a=1;
			  c ="110000";
			
		}
		else if(COMP=="!M"){
			a=1;
			  c ="110001";
			
		}
		else if(COMP=="-M"){
			a=1;
			  c ="110011";
			
		}
		else if(COMP=="M+1"){
			a=1;
			  c ="110111";
			
		}
		else if(COMP=="M-1"){
			a=1;
			  c ="110010";
			
		}
		else if(COMP=="D+M"){
			a=1;
			  c ="000010";
			
		}
		else if(COMP=="D-M"){
			a=1;
			  c ="010011";
			
		}
		else if(COMP=="M-D"){
			a=1;
			  c ="000111";
			
		}
		else if(COMP=="D&M"){
			a=1;
			  c ="000000";
			
		}
		else if(COMP=="D|M"){
			a=1;
			  c ="010101";
		}
		else {
		cout << "Syntax Error";
		return;}
		file << "111";
		file << a;
		file << c;
	}
	void Dest(string DEST){
		string d,j;
		if(DEST=="M"){
			  d ="001";
			  j ="000";
			
		}
		else if(DEST=="D"){
			  d ="010";
			  j ="000";
			
		}
		else if(DEST=="MD"){
			  d ="011";
			  j ="000";
			
		}
		else if(DEST=="A"){
			  d ="100";
			  j ="000";
			
		}
		else if(DEST=="AM"){
			  d ="101";
			  j ="000";
			
		}
		else if(DEST=="AD"){
			  d ="100";
			  j ="000";
			
		}
		else if(DEST=="AMD"){
			  d ="111";
			  j ="000";
			
		}
		else {
		cout << "Invalid Syntax2\n";
		return;
	}	
		file << d;
		file << j<< '\n';
}
	void Jump(string JUMP){
		string d,j;
		if(JUMP=="JGT"){
			  d ="000";
			  j ="001";
			
		}
		else if(JUMP=="JEQ"){
			  d ="000";
			  j ="010";
			
		}
		else if(JUMP=="JGE"){
			  d ="000";
			  j ="011";
			
		}
		else if(JUMP=="JLT"){
			  d ="000";
			  j ="100";
			
		}
		else if(JUMP=="JNE"){
			  d ="000";
			  j ="101";
			
		}
		
		else if(JUMP=="JLE"){
			  d ="000";
			  j ="110";
			
		}
		else if(JUMP=="JMP"){
			  d ="000";
			  j ="111";
			
		}
		else{
		cout << "Invalid Syntax4\n";
		return;
	}
		file << d;
		file << j<< '\n';
}

string spaceremover(string s){
	string st="";
	for(int i=0;i<s.length();i++){
		if(s[i]!=' ' && s[i]!='\t')st.push_back(s[i]);   //if string st is not equal to spaces then store it in string text
	}
	return st;
}

void Assembler(string filename){
	ifstream in;
	string text;
	string ext=".hack";
	string name(filename.begin(),filename.begin()+filename.find('.'));
	string na=name+ext;
	file.open("Test.hack",ios::app);
	in.open(filename.c_str(),ios::in);			//Opening file for reading
	if(in.is_open()){
		while(getline(in,text)){                        //reads the file line by line 
		if(text.find("//") != string::npos)
			text.erase(text.find("//"));            //ignoring comments              
		text=spaceremover(text);                        //removing space function
		A(text);
		C(text);
	}
	in.close();
	file.close();
	rename("Test.hack",na.c_str());
}
}
int main(){
	string filename;
	cout << "Enter filename: ";
	cin >> filename;					//Taking filename from the user
	Assembler(filename);				//Giving it to Assembler Function for further processing
}
