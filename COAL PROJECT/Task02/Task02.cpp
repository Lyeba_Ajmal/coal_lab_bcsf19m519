#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <fstream>
#include <cstring>
#include <conio.h>

using namespace std;

ofstream file;
//Declaring Function Definitions
void Comp(string);
void Dest(string);
void Jump(string);
string spaceremover(string);
void setdefault();
void setlabels();
void AInsruction();
void CInsruction();

//Defining Some Global Variables
string symbol[1000];
unsigned int address[1000];
int i=0,n=16;

//To Set Default Symbols
void setdefault(){
	ifstream in;
	in.open("Default_Symbols.txt",ios::in);
	if(!in.is_open()){
		cout << "Error";
	}
	while(!in.eof()){
		string label;
		in >> label;
		symbol[i]=label;
		int add;
		in >> add;
		address[i]=add;
		i++;
	}
	//i++;
}

//Set Labels
void setlabels(char *filename){
	int count=-1;
	ifstream in;
	string text;
	in.open(filename,ios::in);			//Opening file for reading
	if(!in)cout << "error";
	if(in.is_open()){
		while(getline(in,text)){		//reads the file line by line 
		if(text.find("//") != string::npos)
			text.erase(text.find("//"));		//Removing Comments
		text=spaceremover(text);				//Calling the removespace function o remove spaces
		count++;								
		if(text.empty())count--;
		if(text[0]=='('){
                        count--;
			string label(text.begin()+1,text.begin()+text.find(')'));
			symbol[i]=label;					//Adding the labels in the symbol array and address in address array
			address[i++]=count+1;
	}
	
}
}
}

// A-INSTRUCTION FUNCTION
void A(string text){
	if (text[0]!='@')return;
	if(text.empty())return;
	int b=0, NUM,flag=0;     //Declaring integer type variables NUM & b and initializing b=0
	int BINARY[16];   //Declaring an integer type array of size 16
	string Array(text.begin()+1,text.end());			//Extracting the instrucion after the @ character and sroing in variable "Array
	int binary[16];
	if(atoi(Array.c_str())==0);							//Checking if the code is a number or an label/symbol
	else{NUM=atoi(Array.c_str());goto no;}				//Incase it is a no goto label "no"
	for(int j=0; j<i ; j++){
		flag=0;
		if(Array==symbol[j]){
			NUM=address[j];								//Checking if the label is already present in the symbol table
			flag=1;										//if present its addres is stored in the NUM variable
			break;
		}}
		if(flag==0){
			symbol[i]=Array;							//Incase it is no present the variable and its address is stored in the symbol table
			address[i++]=NUM=n;							
			n++;
		}
		no:
		int array[16]; //Declaring an integer type array of size 16
		for(int i=0; i<16; i++){  //storing 0 in array
		array[i] = 0;
	}
		int c; //declaring integer type variable c
		for (c=0; NUM>0 ; c++){  //converting number from decimal to binary
		BINARY[c] = NUM%2;
		NUM = NUM/2;
	}
		int l, m=15; //Declaring variables l & m and initializing m=15
		for(int l=0; l<c; l++){  
			array[m--] = BINARY[l];  //This loop will be saving the binary of the number in the integer array starting from end
	}
		for (int i=0; i<16; i++){
		file << array[i];	//writing binary in file
	}
		file << '\n';   // printing new line in file
		
	}

// C-INSTRUCTION FUNCTION
void C(string text){
	if(text[0]=='@')return;						//Checking if it is C instruction
	for(int i=0;text[i]!=0;i++){
		if(text[i]=='='){						//Checking if it a dest=comp type funcion
		string DEST(text.begin(),text.begin()+text.find('=')),COMP(text.begin()+text.find('=')+1,text.end());		//Seperating COMP AND DEST 
		Comp(COMP);									//Calling the Comp function
		Dest(DEST);									//Calling the Dest Function
	}
		if(text[i]==';'){						//Checking if it a comp;jump type funcion
			string COMP(text.begin(),text.begin()+text.find(';')),JUMP(text.begin()+text.find(';')+1,text.end());	//Seperating DEST AND JUMP
		Comp(COMP);									//Calling the Comp Function
		Jump(JUMP);									//Calling the Jump Function
		}
	}
	return;
}
	void Comp(string COMP){
		string c;
		int a;
		if(COMP=="0"){
			a=0;
			  c ="101010";							//Matching the Comp function using if statements and in case it matches  
													//Assigning appropriae values to vaiables for writing
		}
		else if(COMP=="1"){
			a=0;
		  c ="111111";
			
		}
		else if(COMP=="-1"){
			a=0;
			  c ="111010";
			
		}
		else if(COMP=="D"){
			a=0;
			  c ="001100";
			
		}
		else if(COMP=="A"){
			a=0;
			  c ="110000";
			
		}
		else if(COMP=="!D"){
			a=0;
			  c ="001101";
			
		}
		else if(COMP=="!A"){
			a=0;
			  c ="110001";
			
		}
		else if(COMP=="-D"){
			a=0;
			  c ="001111";
			
		}
		else if(COMP=="-A"){
			a=0;
			  c ="110011";
			
		}
		else if(COMP=="D+1"){
			a=0;
			  c ="011111";
			
		}
		else if(COMP=="A+1"){
			a=0;
			  c ="110111";
			
		}
		else if(COMP=="D-1"){
			a=0;
			  c ="001110";
			
		}
		else if(COMP=="A-1"){
			a=0;
			  c ="110010";
			
		}
		else if(COMP=="D+A"){
			a=0;
			  c ="000010";
			
		}
		else if(COMP=="D-A"){
			a=0;
			  c ="010011";
			
		}
		else if(COMP=="A-D"){
			a=0;
			  c ="000111";
			
		}
		else if(COMP=="D&A"){
			a=0;
			  c ="000000";
			
		}
		else if(COMP=="D|A"){
			a=0;
			  c ="010101";
			
		}
		else if(COMP=="M"){
			a=1;
			  c ="110000";
			
		}
		else if(COMP=="!M"){
			a=1;
			  c ="110001";
			
		}
		else if(COMP=="-M"){
			a=1;
			  c ="110011";
			
		}
		else if(COMP=="M+1"){
			a=1;
			  c ="110111";
			
		}
		else if(COMP=="M-1"){
			a=1;
			  c ="110010";
			
		}
		else if(COMP=="D+M"){
			a=1;
			  c ="000010";
			
		}
		else if(COMP=="D-M"){
			a=1;
			  c ="010011";
			
		}
		else if(COMP=="M-D"){
			a=1;
			  c ="000111";
			
		}
		else if(COMP=="D&M"){
			a=1;
			  c ="000000";
			
		}
		else if(COMP=="D|M"){
			a=1;
			  c ="010101";
		}
		else {
		cout << "Syntax Error";
		return;}
		file << "111";
		file << a;										//Writing in file
		file << c;
	}
	void Dest(string DEST){
		string d,j;
		if(DEST=="M"){
			  d ="001";										//Matching the Dest function using if statements and in case it matches 
			  j ="000";										// Assigning appropriae values to vaiables for writing
			
		}
		else if(DEST=="D"){
			  d ="010";
			  j ="000";
			
		}
		else if(DEST=="MD"){
			  d ="011";
			  j ="000";
			
		}
		else if(DEST=="A"){
			  d ="100";
			  j ="000";
			
		}
		else if(DEST=="AM"){
			  d ="101";
			  j ="000";
			
		}
		else if(DEST=="AD"){
			  d ="100";
			  j ="000";
			
		}
		else if(DEST=="AMD"){
			  d ="111";
			  j ="000";
			
		}
		else {
		cout << "Invalid Syntax2\n";
		return;
	}	
		file << d;
		file << j<< '\n';								//Writing to file
}
	void Jump(string JUMP){
		string d,j;
		if(JUMP=="JGT"){
			  d ="000";
			  j ="001";									//Matching the Comp function using if statements and in case it matches 
														//Assigning appropriae values to vaiables for writing
		}
		else if(JUMP=="JEQ"){
			  d ="000";
			  j ="010";
			
		}
		else if(JUMP=="JGE"){
			  d ="000";
			  j ="011";
			
		}
		else if(JUMP=="JLT"){
			  d ="000";
			  j ="100";
			
		}
		else if(JUMP=="JNE"){
			  d ="000";
			  j ="101";
			
		}
		
		else if(JUMP=="JLE"){
			  d ="000";
			  j ="110";
			
		}
		else if(JUMP=="JMP"){
			  d ="000";
			  j ="111";
			
		}
		else{
		cout << "Invalid Syntax4\n";
		return;
	}
		file << d;
		file << j<< '\n';								//Writing to file
}

string spaceremover(string s){
	string st="";
	for(int i=0;i<s.length();i++){
		if(s[i]!=' ' && s[i]!='\t')st.push_back(s[i]);								//Function removes spaces and only returns he string containing the code
	}
	return st;
}

void Assembler(string filename){
	ifstream in;
	string text;
	string ext=".hack";
	string name(filename.begin(),filename.begin()+filename.find('.'));				//Extracting the filename without extension 
	string na=name+ext;																//Adding .hack extension at the end of filename
	file.open("Test.hack",ios::out);												//Opening output file Test.hack
	in.open(filename.c_str(),ios::in);			//Opening file for reading
	if(in.is_open()){
		while(getline(in,text)){//reads the file line by line 
		if(text.find("//") != string::npos)
			text.erase(text.find("//"));											//To ignore comments using erase and find function
		text=spaceremover(text);													//Calling removespaces function to remove spaces
		A(text);															//Calling the AInstruction
		C(text);															//Calling the CInstruction
	}
	in.close();
	file.close();																	//Closing the files
	rename("Test.hack",na.c_str());													//Renaming the test.hack file to new filename including the .hack extension
}
}
int main(){
	cout << "Enter filename: ";
	char filename[20];
	cin >> filename;					//Taking filename from the user
	setdefault();
	setlabels(filename);
	Assembler(filename);				//Giving it to Assembler Function for further processing
	cout << "\n\n\nThe Program has been Assembled..";
}
