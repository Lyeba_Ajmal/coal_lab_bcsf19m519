//Program: for (i=0; i<n; i++)
//Compute: stores whole numbers from 0 to n-1 in an array of size n array starting at address 21
// initialize arr =20, n=0 , i=0

// arr = 20

@20  // 20 is data value so A=20 because A is used to hold data values or address of memory
D=A  // D=20 because D is also used to hold data value
@arr // user defined variable arr starting at RAM 16  
M=D  // storing value of arr = 20 ar RAM 16

@R0 // virtual register having memory address 0
D=M // D = RAM[0] reading memory address of RAM
@n  // new variable n having memory address 17
M=D // n = RAM[0] and value of n is 0 at address 17

@i  // new variable i having memory address 18
M=0 // i=0


(LOOP) // declaring a label LOOP

// if (i==n) goto END

@i    //value of i that is 0
D=M   // reading value of i such that i=0
@n    //value of n
D=D-M // subtracting  value of i from n and storing the new value in n
@END  // reffering to a label i.e END
D;JEQ // if i becomes equal to n then jump to END otherwise sequence

// RAM [arr + i] = A-D
@arr  // variable arr
D=M   // reading value of arr that is 20
@i    // variable i 
A=D+M // A = 0 + 20 adding value of i and arr in A
M=A-D // RAM [arr + i] = A-D 

// i++
@i    // value of i i.e 0
M=M+1 // incrementing the value of i by adding 1
@LOOP // reffering to loop
0;JMP // jump unconditionally goto loop

(END) // label END
@END  // reffering to label END
0;JMP // jump unconditionally to terminate the program (infinite loop)