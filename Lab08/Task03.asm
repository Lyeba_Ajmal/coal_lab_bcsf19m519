// 

this program prints horizontal line on screen of 16 bits, the number of line are decided by the value of n varialbe

such that if n=3 then the count of printed line will b 1 value more then "n" so there will b 3 horizontal lines



//



// n = RAM[0]



@R0	// activating RAM[0] location

D=M	// coping content of RAM[0] to D register

@n	// declaring a varible n

M=D	// coping content of RAM[0] in varible n



// i=0



@i	// declaring a variable i

M=0	// puting 0 in i variable



// addr = SCREEN



@SCREEN	// activationg starting location of screen memory map

D=A	// coping A to D

@addr	// declaring addr variable

M=D	// coping D to M



//loop body (label)
 


(LOOP)



//loop condition i<=n



@i	// activating memory location of i variable

D=M	// value of i variable to D register

@n	// activating memory location of n variable

D=D-M	// substracting value of n varibale from D register's vale (i-n)

@END	// activating ROM location of END label

D;JGT	// jumping to END label if D is grater then 0



// turning ON 16 bits on screen
 

@addr	// activating addr

A=M	// activating memory location of addr variable's value

M=-1	// putting (1111111111111111) at memory location RAM[addr]



// incrementing i


@i	// activating location of i

M=M+1	// addinting 1 at activated memory location



// movig 2 row down on screen


@64	// 64 in A register

D=A	// A to D register

@addr	// activating addr memory location 

M=D+M   // adding value of A at memory location of addr



// unconditional jump to the start of body 



@LOOP	// activating LOOP label in ROM

0;JMP	// unconditional jump to LOOP



// label END

(END)

@END	// activating ROM location of END label

0;JMP	// unconditional jump to END label